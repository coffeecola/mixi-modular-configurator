var gulp = require('gulp');
var fs = require('fs');
var rename = require('gulp-rename');
// var plumber = require('./plumber.js').onError;
var sass = require('gulp-sass');
var autoprefix = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');
var cssnano = require('gulp-cssnano');
var server = require ('./server.js').instance;
var gutil = require('gulp-util');
var base64 = require('gulp-base64');
var size = require('gulp-size');

module.exports = function (path) {
    gulp.task('style', function () {
        return gulp.src(path.style)
            // .pipe(plumber())
            .pipe(sourcemaps.init())
            .pipe(sass({outputStyle: 'expanded'})
                .on( 'error', function(err) {
                    gutil.log(gutil.colors.red('Error:'), err.message);
                    this.emit('end');
                })
            )
            .pipe(base64({ debug: true }))
            // .pipe(rename('./dist/style.min.css'))
            .pipe(autoprefix('last 2 versions'))
            // Sass compresses does not create proper source maps
            // See issue https://github.com/sass/node-sass/issues/957
            .pipe(cssnano())
            .pipe(sourcemaps.write())
            .pipe(size({showFiles:true}))
            .pipe(gulp.dest(path.dest))
            .pipe(server.stream());
    });
};
