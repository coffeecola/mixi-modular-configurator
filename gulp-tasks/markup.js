var gulp = require('gulp');
var pug = require('gulp-pug');
var size = require('gulp-size');
var plumber = require('./plumber.js').plumber;

var sizeOpt = {
  showFiles: true,
  showTotal: false
};
module.exports = function (path) {
  gulp.task('markup', function () {
    return gulp.src([ path.markup, '!**/fragments/*', '!**/production*.pug' ])
      .pipe(plumber())
      .pipe(pug())
      .pipe(size(sizeOpt))
      .pipe(gulp.dest(path.dest));
  });
  gulp.task('dist-blobs', function () {
    return gulp.src(path.markup + 'production*.pug')
      .pipe(plumber())
      .pipe(pug())
      .pipe(size(sizeOpt))
      .pipe(gulp.dest(path.dest));
  });
};
