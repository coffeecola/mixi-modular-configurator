var fs = require('fs');
var gulp = require('gulp');
var ftp = require('vinyl-ftp');
var gutil = require('gulp-util');

module.exports = gulp.task( 'ftp', function () {

    // load config data
    var config = JSON.parse( fs.readFileSync('./ftp-config.json') );

    var connection = new ftp({
        host:     config.host,
        user:     config.user,
        password: config.password,
        parallel: 10,
        log:      gutil.log
    });

    var globs = [
        '!(.DS_store)',
        './dist/*'
    ];
    return gulp.src( globs, { buffer: false})
        .pipe( connection.newer( '/fringe-studio' )) //only upload newer files
        .pipe( connection.dest( '/fringe-studio' ));
});
