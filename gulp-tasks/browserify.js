'use strict';

var gulp = require('gulp');
// var onError = require('./plumber.js').onError;
var uglify = require('gulp-uglify');
var browserify = require('browserify');
var watchify = require('watchify');
var sourcemaps = require('gulp-sourcemaps');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var size = require('gulp-size');
var gutil = require('gulp-util');

var onError = function(err, file) {
  gutil.log(
    gutil.colors.red("Browserify compile error:"+'\n'),
    err.message.replace(/'.+?'/g, function(str){
      return gutil.colors.blue(str);
    })
  );
  gutil.beep();
  this.emit('end'); // Ends the task
};

module.exports = function (path) {

  var bundler = browserify({
    entries: [path.js],
    debug: true, // creates sourcemaps
    // for Watchify
    cache: {},
    packageCache: {}
    //shim for jquery (since its provided on their CDN)
  }).transform({global: true}, 'browserify-shim');

  // the main Gulp pipeline
  function bundle() {
    return bundler
      .bundle()
      .on( 'error', onError)
      .pipe(source('bundle.js'))
      .pipe(buffer())
      .pipe(sourcemaps.init({loadMaps: true}))
      // .pipe(plumber())
      .pipe(uglify())
      .pipe(sourcemaps.write())
      .pipe(size({showFiles:true}))
      .pipe(gulp.dest(path.dest));
  }

  gulp.task('js', bundle); // run browserify without watchify

  gulp.task('watchify' , function () {
    var b = watchify(bundler);

    b.on('update', bundle); // trigger buid (instead of gulp.watch())
    b.on('time', function (time) { // output build logs to terminal
      var c = gutil.colors;
      gutil.log(c.blue('Watchify'), 'finished in', c.magenta(time, 'ms'));
    });
    bundle();
  });
};
