var gulp = require('gulp');
var imagemin = require('gulp-imagemin');
var plumber = require('./plumber.js').plumber;

module.exports = function (path) {
    gulp.task( 'img', function () {
        return gulp.src(path.woodgrain)
          .pipe(plumber())
          .pipe(imagemin({
            progressive: true
        }))
        .pipe(gulp.dest(path.optimized));
    });
};
