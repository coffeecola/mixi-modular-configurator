var gulp = require('gulp');
var browserSync = require('browser-sync').create();

module.exports = {
    instance: browserSync, // return this instance css injection  in `style.js`
    server: function (path) {
        gulp.task('serve', function() {
            browserSync.init({
                open: false,
                server: {
                    baseDir: path.dest,
                    index: 'index.html'
                }
            });
            gulp.watch( path.dest + '*.html' ).on('change', browserSync.reload );
            gulp.watch( path.dest + '*.js' ).on('change', browserSync.reload );
        });
    }
};
