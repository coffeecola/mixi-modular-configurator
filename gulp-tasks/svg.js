var gulp = require('gulp');
var gulpif = require('gulp-if');
var plumber = require('./plumber.js').plumber;
var debug = require('gulp-debug');
var changed = require('gulp-changed');
var gulpif = require('gulp-if');
var lazypipe = require('lazypipe');
var rename = require('gulp-rename');
var replace = require('gulp-replace');
var svgToCss = require('gulp-svg-to-css');
var svgmin = require('gulp-svgmin');

// Utility function for gulp-if
// replace classes and colors in SVGs
function addClass ( filePath, color, css ) {
    var fill = 'fill="' + color + '"';
    var cssClass = 'class="'+ css + '"';
    var svgTranforms = lazypipe()
      .pipe(replace, fill, function (match) {
          return cssClass + ' ' + match.replace( color, 'currentColor');
      });
    return gulpif( filePath, svgTranforms() );
}

module.exports = function (path) {

    // Optimize SVGs
    // Replace color is replaced with classes and colors
    gulp.task('svg', function () {

        // gulp-if filePath filters
        var inserts = 'bases/*.svg';
        var woodgrain = '{boxes,accessories}/*.svg';
        var centerDivide = 'boxes/open*42*.svg';

        return gulp.src(path.svg)
            .pipe(plumber())
            // TODO gulp-changed doesn't seem to be working
            // .pipe(changed('./src/img/optimized'))
            .pipe(replace('id="shadow"', 'class="shadow hide"')) // shadow for wineracks
            .pipe(replace('id="lid"', 'class="lid"'))           // class for trashcan lid
            .pipe(addClass(woodgrain, '#0000FF', 'bkg')) // add bkg class for woodgrain
            .pipe(addClass(inserts, '#FF0000', 'inset'))   // add inset class
            .pipe(replace('#00FF00', 'currentColor'))
            .pipe(svgmin())
            .pipe(gulp.dest(path.optimized));
    });
};
