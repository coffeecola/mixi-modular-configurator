var plumber = require('gulp-plumber');
var notify = require('gulp-notify');
    // TODO: overhaul the error handler
    // doesn't work with browserify or sass
    onError = function(err) {
      notify.onError({
          title:    "Gulp",
          subtitle: "Failure!",
          message:  "Error: <%= error.message %>",
          sound:    "true"
      })(err);
      this.emit('end');
    };

module.exports = {
  onError: function () {
    return onError();
  },
  plumber: function () {
    return plumber({ errorHandler: onError });
  }
};