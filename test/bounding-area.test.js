var test = require('tap').test;
var outerBounds = require('../src/js/bounding-area.js');

// simulate boundingClientRect() obj for two DOM nodes
var singleRect = [ { left: 10, top: 10, width: 20, height: 20 } ];
var multiRects = [
    { left: 40, top: 20, width: 10, height: 10 },
    { left: 60, top: 40, width: 10, height: 10 },
    { left: 20, top: 10, width: 10, height: 10 },
    { left: 10, top: 60, width: 10, height: 10 }
];
test( 'Test outer boundary on empty array', function (t) {
    var outer = outerBounds({});
    t.ok( outer instanceof Object,'outerBounds() returns an object');
    t.equal( Object.keys(outer).length, 0, 'Object is empty');
    t.end();
});
test( 'Test outer boundary of single node', function (t) {
    var outer = outerBounds(singleRect);
    t.equal( outer.left,   10, 'Closest point to the left is 10px');
    t.equal( outer.top,    10, 'Closests point to the top is 10px');
    t.equal( outer.width,  20, 'Boundary width is 20px');
    t.equal( outer.height, 20, 'Boundary height is 20px');
    t.end();
});
test( 'Test outer boundary of node collection', function (t) {
    var outer = outerBounds(multiRects);
    t.equal( outer.left,   10, 'Closest point to the left is 10px');
    t.equal( outer.top,    10, 'Closest point to the top is 10px');
    t.equal( outer.width,  60, 'Boundary width is 60px');
    t.equal( outer.height, 60, 'Boundary height is 60px');
    t.end();
});
