var test = require('tap').test;
require("jsdom").env("", function(err, window) {
    if (err) {
        console.error(err);
        return;
    }

    var $ = require("jquery")(window);
    var doc = window.document;

    test('jquery works', function (t) {
        t.plan(1);
        t.ok( $, 'jQuery exists');
    });
});


