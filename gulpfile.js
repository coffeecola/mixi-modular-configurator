var gulp = require('gulp');
var sequence = require('run-sequence');

//————————————————————————————————————————————————————————————
var path = {
  //  * * * * IN * * * * *
  js: './src/js/index.js',
  svg: './src/img/assets/**/*.svg',
  style: './src/style/**/*.scss',
  woodgrain: './src/img/assets/wood-patterns/*.jpg',
  markup: './src/markup/**/*',
  // * * * * * OUT * * * * *
  optimized: './src/img/optimized/', // optimized images are embedded into CSS/HMTL
  dest: './dist/'
};

//————————————————————————————————————————————————————————————
// Gulp task modules
require('./gulp-tasks/ftp.js');
require('./gulp-tasks/server.js').server(path);
require('./gulp-tasks/style.js')(path);
require('./gulp-tasks/markup.js')(path);
require('./gulp-tasks/svg.js')(path);
require('./gulp-tasks/img.js')(path);
require('./gulp-tasks/browserify.js')(path);

//————————————————————————————————————————————————————————————

// run browserify
gulp.task('build:js', ['js']);
// optimize SVGs,inline them into templates, and render to HTML
gulp.task('build:html', function(){
  return sequence('svg', 'markup');
});
// optimize pattern jpegs, compile Sass and base64 images into CSS
gulp.task('build:css', function(){
  return sequence('img', 'style');
});
// run build CSS and JS in parrallel, then create distribution blobs
// used for copy/pasting into Weebly WYSIWYG editor  >_<
gulp.task('build:blobs', function(){
  return sequence( ['build:css', 'build:js'], 'dist-blobs');
})
gulp.task('build', ['build:js', 'build:html', 'build:css']);

//————————————————————————————————————————————————————————————
// `watch` is starts up `watchify` as a dependent task, since browserify
// has it's own watcher which does a better job at caching the changes.
// https://github.com/gulpjs/gulp/blob/master/docs/recipes/fast-browserify-builds-with-watchify.md
gulp.task('watch', ['watchify'], function () {
  // watch CSS ( updated inject into browserSync server )
  gulp.watch(path.style, ['style']);
  // compile pug templates, including when svg-sprite changes
  gulp.watch(path.markup, ['markup']);
  // optimize SVG
  gulp.watch(path.svg, ['build:html']);
  // optimize woodgrain sprite &  encode into Base64 for CSS
  gulp.watch(path.woodgrain, ['build:css']);
});

//————————————————————————————————————————————————————————————
// Default task depends on watch being declared
gulp.task('default',['watch', 'serve']);
