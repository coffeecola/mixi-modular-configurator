var $ = require('./j-weebly.js');
var onCanvas = {};

//TODO: write a test for this module
module.exports = {
    onCanvas: onCanvas,
    add: record,
    delete: deleteRecorded
};

function updateData (draggable, data) {
    /* Create an object of the dropped element's parameters.
     * On the 1st drop, record data that is constant.
     * On subsequent drops, only record changing data.
     */
    if ( !Object.keys(data).length ) {
        data.name   = draggable.attr('data-name');
        data.color  = draggable.attr('data-color');
        data.inset  = draggable.attr('data-inset') || '';
        data.width  = draggable.width();
        data.height = draggable.height();
    }
    data.parent = draggable.parent().attr('id') || draggable.parents('.dropped-item').attr('id');
    data.left = parseFloat(draggable.css('left'));
    data.top = parseFloat(draggable.css('top'));
    return data;
}

function record (draggable) {
    var key = draggable.attr('id').substr(5);
    var dropData = onCanvas[key] || {};
    onCanvas[key] = updateData(draggable, dropData);
}

function deleteRecorded (draggable) {
    var accessories, keys = [];
    var isJQuery = draggable instanceof $;

    if ( !isJQuery ) draggable = $(draggable);
    keys.push( draggable.attr('id').substr(5) );
    accessories = draggable.find('.dropped-item');

    // Add any accessories that might have been inside the item
    // to the collection of items to be deleted from the record
    if ( accessories.length ) accessories.each(function (_, acc) {
            keys.push( $(acc).attr('id').substr(5) );
    });
    keys.forEach( function (key) {
        delete onCanvas[key];
    });
}
