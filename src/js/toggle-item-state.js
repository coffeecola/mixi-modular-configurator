var $ = require('./j-weebly.js');
var intro = $('#intro');
// TODO: move trashcan into a different module
var trashcan = $('.trashcan');

module.exports = function (e, ui) {
    var el = ui.helper;
    var context = $(el.context);
    var indexLvl = e.type === 'dragstart' ? '10' : '';
    var node = el.parent().hasClass('.dropped-item') ?
        el.parent('.dropped-item') : el;
    /**
     * When dragging a piece of furniture, z-index it to the top, unless
     * it is an accessory inside of a box, in which case it is necessary
     * to z-index it's parent (so that it will appear above all off the
     * other dropped-items.
     */
    node.css('z-index', indexLvl);

    // Selection panel items
    if ( el.hasClass('panel-item') ) {
        trashcan.toggleClass('dim');
        // TODO: cleaner way to disable trashcan?
        trashcan.droppable('disable');
        context.toggleClass('dim');
        // hover is added, since mouseEvents don't transfer to ui.helpers
        el.toggleClass('hover');
    } else {
        trashcan.droppable('enable');
    }
    // accessories
    // if draggable is an accessory,
    // dim elements that don't correspond to it
    // TODO: highlighting for matches, no dim for standalones
    // if (  el.attr('data-acc') ) {
    //     console.log(onCanvas.length );
    //   if ( onCanvas.length )
    //     onCanvas.not( match ).toggleClass('dim');
    //   if ( !match ) {
    //     noMatchMsg.toggleClass('hide');
    //     if ( msg) noMatchMsg.text( msg );
    //   }
    // }
};
