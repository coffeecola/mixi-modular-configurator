module.exports = boundingArea;

/**
 * B O U N D I N G   A R E A
 * * * * * * * * * * * * * *
 * Get the bounding area of a collection of DOM nodes.
 * Input an array of objects containing each nodes
 * position (left, top) and dimensions (width, height).
 * Return an object in the same format.
 */

function boundingArea (arr) {
    var boundary = {};
    function farSide(obj, side) {
        return side === 'left' ? obj[side] + obj.width : obj[side] + obj.height;
    }
    if ( !arr.length ) {
        return boundary;
    } else if ( arr.length === 1 ) {
        boundary.left    = arr[0].left;
        boundary.top     = arr[0].top;
        boundary.farLeft = farSide(arr[0], 'left');
        boundary.farTop  = farSide(arr[0], 'top');
    } else {
        boundary = arr.reduce( function (a,b) {
            if ( !a.farLeft ) a.farLeft = farSide(a, 'left');
            if ( !a.farTop )  a.farTop  = farSide(a, 'top');
            b.farLeft = farSide(b, 'left');
            b.farTop  = farSide(b, 'top');
            // filter for smallest and furthest distances from left and top
            return {
                left: a.left < b.left ? a.left : b.left,
                top: a.top < b.top ? a.top : b.top,
                farLeft: a.farLeft > b.farLeft ? a.farLeft : b.farLeft,
                farTop: a.farTop > b.farTop ? a.farTop : b.farTop
            };
        });
    }
    boundary.width = boundary.farLeft - boundary.left;
    boundary.height = boundary.farTop - boundary.top;
    delete boundary.farLeft;
    delete boundary.farTop;
    return boundary;
}

