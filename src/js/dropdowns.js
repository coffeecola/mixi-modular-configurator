var $ = require('./j-weebly.js');
////////////
// DROPDOWNS
////////////
var dropdowns = $('.dropdown');
var ul = dropdowns.find('.dd-list ul');
// change the color and attr of furniture
function changeColor (elements, attr, newColor) {

    elements.attr( 'data-' + attr, newColor );

    // change only the color inset on bases
    if ( attr === 'inset' )
        elements = elements.find('.inset');

    elements.each( function (_, el) {
        var colors = [
            'pewter',
            'brass',
            'bronze',
            'white',
            'taupe',
            'gray',
            'maple',
            'oak',
            'walnut',
            'charcoal'
        ];
        // using Vanilla JS since jQuery doesn't work w/ SVG
        colors.forEach( function (oldColor) {
            if( el.classList.contains(oldColor) ) {
                el.classList.remove(oldColor);
            }
            el.classList.add(newColor);
        });
    });
}


function furnitureAttr ( selection ) {
    selection = selection instanceof $ ? selection : $(selection) ;
    var furniture = selection.attr('data-furniture');
    var boxType = selection.attr('data-boxtype');
    selection = $(selection);

    if (boxType) { // change box panel
        var panels = $('#boxes .panel');
        var selected = $('#' + boxType);
        panels.removeClass('active');
        selected.addClass('active');
    } else { // change color
        var nodes = $('.panel-item' + '[data-furniture='+furniture+']' );
        var attr = selection.attr('data-inset') ? 'inset' : 'color' ;
        var color = selection.attr('data-inset') || selection.attr('data-color');
        changeColor(nodes, attr, color);
    }
        // toggle the three box panels (open, drawer, hinged)
}
// Show the new value in the dropdown
// once user has selected from the dd list
function dropdownVal (liDiv) {
    var selectedItem = $(liDiv);
    var readOutValue = selectedItem.parents(ul).siblings('.dd-current-value');
    var list = readOutValue.siblings('ul');
    var listItems = ul.find('li div');

    listItems.removeClass('active');
    list.removeClass('active');
    readOutValue.html( selectedItem.html() );
    selectedItem.addClass('active');
}

/////////////////
// INITIAL VALUES
ul.each( function(i, list) {
    var defaults = $(list).find('li div').first();

    dropdownVal (defaults);
    furnitureAttr (defaults);
});

//////////////////
// EVENT HANDLERS

// Open Dropdowns
dropdowns.on('click', function (e) {
    // if another dropdown is active, close it
    var thisUl = $(this).find(ul);
    var otherUl = ul.not(thisUl);

    otherUl.removeClass('active');
    thisUl.toggleClass('active');
    return false; // to stop document click eventHandler
});
// when user clicks dropdown option
ul.on('click', 'li div', function (e) {
    dropdownVal(this);
    furnitureAttr(this);
    return false;
});
// close if user clicks outside dd
// mousedown in case someone goes straight to dragging
$(document).click(function () {
    ul.removeClass('active');
});
