// Data used to set all draggable elements
module.exports = [
    //  type:  accessory's selector
    //  snap:  draggable snaps to this/these selector(s)
    //  msg:   text for msg when no corresponding box is on the canvas. msg reads as:
    //         'This accessory requires' + msg
    /////////////
    { // Bases and Boxes
        type: '.panel-item',
        snap: ':not([data-acc="standard"])',
        snapMode: 'outer',
        snapTolerance: 12,
        mainSetting: true
    },
    /////////////
    { // Harware
        type: '[data-furniture="hardware"]',
        snap: '[data-closed-box]', // includes hanging files
        snapMode: 'inner',
        snapTolerance: 6,
        msg: 'a box with hinges or drawers'
    },
    /////////////
    // Pull-out Desks
    { // 28" Wide
        type: '[data-name="28 in. Pull-out Desk"]',
        snap: '[data-name$="Box"]:not([data-name^="7"], [data-name^="14"], [data-name^="21"]) .dropbox', // (28" or 42") open box
        snapMode: 'inner',
        snapTolerance: 6,
        msg: 'an open box that is at least 28" wide'
    }, { // 42" Wide
        type: '[data-name="42 in. Pull-out Desk"]',
        snap: '[data-name^="42"] .dropbox', // any 42" wide box
        snapMode: 'inner',
        snapTolerance: 6,
        msg: 'an open box that is 42" wide'
    },
    /////////////
    // Wine Racks
    { //  14" Wide
        type: '[data-name="14 x 14 in. Wine Rack"]',
        standalone: true,
        snap: '[data-name$="14 in. Box"] .dropbox', // any 14" tall open box
        snapMode: 'inner',
        snapTolerance: 6,
        // msg: 'an open box that is 14" tall'
    }, { //  21" Wide
        type: '[data-name="21 x 14 in. Wine Rack"]',
        standalone: true,
        snap: '[data-name$="14 in. Box"]:not([data-name^="14 x"] ) .dropbox', //all open boxes 14" tall, except 14" wide
        snapMode:  'inner',
        snapTolerance: 6,
        msg: 'an open box that is 14" tall and at least 21" wide'
    }, { // 28" Wide
        type: '[data-name="28 x 14 in. Wine Rack"]',
        standalone: true,
        snap: '[data-name="28 x 14 in. Box"] .dropbox',
        snapMode:  'inner',
        snapTolerance: 6,
        msg: 'the 28" x 14" open box'
    },
    /////////////
    // Speakers
    { // Horizontal
        type: '[data-name="Horizontal Speaker"]',
        standalone: true,
        snap: '[data-furniture="box"]:not([data-name="7 x 17.5 in. Box"]) .dropbox',
        snapMode:  'inner',
        snapTolerance: 6
        // msg: 'an open box that is at least 14" wide'
    }, { // Vertical
        type: '[data-name="Vertical Speaker"]',
        standalone: true,
        snap: '[data-furniture="box"]:not([data-name~="x 7 in. Box"], [data-name~="10.5 in."]) .dropbox',
        snapMode: 'inner',
        snapTolerance: 6
        // msg: 'an open box that is 14" tall'
    }
];
