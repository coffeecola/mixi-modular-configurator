var $ = require('./j-weebly.js');

require('jquery-ui/draggable');
require('jquery-ui-touch-punch');

var dragData = require('./data/drag-opts.js');
var toggleUiState = require('./toggle-item-state.js');

// input a querySelector
// returns a jquery object
// bound with jquery-ui draggable
module.exports = function (querySelector) {
    //store main settings for standalone accessories
    var mainSet;
    var elements = $(querySelector);
    // filter for all matching accessories, then set drag opt
    dragData.forEach(function (set) {
        var el = elements.filter(set.type);
        var options = {
            helper: 'clone',
            appendTo: '.dropzone',
            snap: '.dropped-item' + set.snap,
            snapMode: set.snapMode,
            snapTolerance: set.snapTolerance,
            revert: 'invalid',
            revertDuration: 200,
            start: toggleUiState,
            stop: toggleUiState
        };
        var snapSet = [ 'innerSnap', 'snap', 'outerSnap'];
        // set special snap values for standalone accessories
        // values are toggled when dragged over a corresponding droppable box
        function setStandalone (el, i) {
            var setting = i < 1  ? set : mainSet;
            options[ el ]               = '.dropped-item' + setting.snap;
            options[ el + 'Mode' ]      = setting.snapMode;
            options[ el + 'Tolerance' ] = setting.snapTolerance;
        }
        if ( set.mainSetting ) mainSet = set;
        if ( set.standalone )  snapSet.forEach(setStandalone);
        el.draggable(options);
    });
    return elements;
};
