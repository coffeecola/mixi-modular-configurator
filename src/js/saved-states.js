var $ = require('./j-weebly.js');
var hashTable = require('./data/furniture-hash-table.js');
// LOCAL STORAGE
// // check for localStorage
var storageAvailable;
try {
    var x = '__storage_test__';
    window.localStorage.setItem(x, x);
    window.localStorage.removeItem(x);
    storageAvailable = true;
}
catch(e) {
    storageAvailable = false;
}

// check if first-time or returning visitor
if ( storageAvailable ) {
    if ( !localStorage.getItem('visited') )
        localStorage.setItem('visited', true);
    else {
        var storageKeys = Object.keys(localStorage);
        var onCanvas = storageKeys.filter( function(key) {
            return !isNaN(key);
        }).map( function (key) {
            return JSON.parse( localStorage[key] );
        });
    }
}
// Check if shared or saved link
// URI Hash has precedence over what is stored in localStorage
if ( window.location.length > 0 ) {
    onCanvas = uri.decode();
}

// find matching from the selection panel
if ( onCanvas.length > 0 ) {
    var matchedItems = $();
    var fragment = document.createDocumentFragment();
        // find match
    onCanvas.forEach( function (obj) {
        var match = $('.panel-item').filter('[data-name="' + obj.name + '"]');
        matchedItems = matchedItems.add(match);
    });
    // add style
    matchedItems.toArray().forEach( function (el, i){
        var e = $(el);
        // var drag = e.draggable('option');
        // var obj = onCanvas[i];
        // el.setAttribute( 'id', 'drop-' + i );
        // el.style.position = 'absolute';
        // el.style.left =  obj.pos.left;
        // el.style.top = obj.pos.top;

        if ( obj.parent === 'dropzone' ) e.appendTo(fragment);
        else fragment.querySelector(obj.parent).appendChild(e[0]);
    });

    $(fragment).appendTo('.dropzone');
}

// - - - - - - - - - - - -  - - - - - -  - - - - - -

// return an array (or array[el]) of each hash stored in the URI
function uriArr (i) {
    var hash = window.location.hash;
    var arr = hash.length ? hash.slice(2).split('!') : [];
    return i === undefined ? arr : arr[i];
}

// hash and unhash data-attributes
function hash (arg) {
    var keys = Object.keys(uriHash);
    var values = keys.map( function (key) {
        return uriHash[key];
    });
    // unhash
    if ( uriHash.hasOwnProperty(arg) )
        return uriHash[arg];
    // hash
    else {
        return keys.filter( function (key) {
            return uriHash[key] === arg;
        }).toString();
    }
}
// turn drop element into hashCode
//if panel-item
    // append to hash
// take current hash,
// turn into array
    // test each item against hashCode.id
    // if it matches, replace
    //




// module.exports = {
//   decode: function () {
//     if (storageAvailable) {

//     }
//   },
//   remove: function () {
//     if (storageAvailable) {

//     }
//   },
//   record: function (el) {
//     var id = Number(el.attr('id').split('-').pop());
//     var color = hash( el.attr('data-color') );
//     var type = hash ( el.attr('data-name') );
//     var left = parseFloat( el.css('left') );
//     var top = parseFloat( el.css('top') );
//     var hashString = color + type + '-' + left + ',' + top;
//     var uri = uriArr();

//     if ( uriArr(id) ) {
//       uri[id] = hashString;
//       console.log( 'id match:', id, uri);
//     }
//     else {
//       uri.push( hashString);
//       console.log( 'push:', uri);
//     }
//     window.location.hash = '#!' + uri.join('!');
//   }
// };
// // init
// $(document).ready( function () {
//   // debugging
//   window.location.hash = '';

//   // if ( window.location.hash.length ) {
//   //   // parse hash string
//   // }
// });
