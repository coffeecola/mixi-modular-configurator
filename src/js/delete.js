var $ = require('./j-weebly.js');
var rulers = require('./rulers.js');
var record = require('./recorder.js');
var trashcan = $('.trashcan');
var trashLid = document.querySelector('.lid'); // jQuery has bad SVG support

module.exports = {
    hide: hideTrash,
    show: showTrash,
    open: openLid,
    close: closeLid
};

function hideTrash () {
    trashcan.addClass('hide');
}
function showTrash () {
    trashcan.removeClass('hide');
}
function openLid () {
    trashLid.classList.add('lid-up');
}
function closeLid () {
    trashLid.classList.remove('lid-up');
}

var deleteDraggable = function (ui, animationName) {
    var transitionEnd = 'transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd';
    ui.draggable
      .addClass(animationName)          // add delete animation
      .on( transitionEnd, function () { // wait for animation to end
            closeLid();
            $(this).remove();
            record.delete(this);
            rulers.set(record.onCanvas);
            if ( Object.keys(record.onCanvas).length === 0 ) {
                hideTrash();
            }
      });
};
// delete when user returns item to selection panel
$('.selection-panel').droppable({
    accept: '.dropped-item',
    greedy: true,
    tolerance: 'pointer',
    over: function ( e, ui ) {
        ui.helper.addClass('delete');
    },
    out:  function ( e, ui ) {
        ui.helper.removeClass('delete');
    },
    drop: function ( e, ui ) {
        deleteDraggable(ui, 'put-it-back');

    }
});
trashcan.droppable({
    greedy: true,
    tolerance: 'touch',
    over: function (e, ui) {
        ui.helper.addClass('delete');
        openLid();
    },
    out: function (e, ui) {
        ui.helper.removeClass('delete');
        closeLid();
    },
    drop: function (e, ui) {
        deleteDraggable(ui, 'deleting');
    }
});
