var $ = require('./j-weebly.js');
require('jquery-ui/draggable');
var count = require('./count.js');

module.exports = function (original, draggableOptions) {
    count.add();
    draggableOptions.helper = 'original';

    return original.clone(true)
      .css('z-index', '') // drag z-indexing doesn't carry over when cloned
      .attr('id', 'drop-' + count.val())
      .removeClass('panel-item hover dim ui-draggable-dragging')
      .addClass('dropped-item')
      // reapply this drag settings (does not survive cloning)
      .draggable(draggableOptions);
};
