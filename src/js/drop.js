var $ = require('./j-weebly.js');
require('jquery-ui/droppable');

var dragData = require('./data/drag-opts.js');
var clone = require('./clone.js');
var count = require('./count.js');
var trash = require('./delete.js');
var rulers = require('./rulers.js');
var record = require('./recorder.js');

// HELPER FUNCTIONS
// map an array and delete empty entries
function mapPrune (array, callback) {
    return array.map(callback)
        .filter( function (el) {
            return el;
        });
}
// calculate new positon of accessory
// when moved between dropbox and dropzone
function resetPosition (ui, droppable) {
    if ( droppable instanceof $ === false )
        droppable = $(droppable);

    var dragPos = ui.offset;
    var dropPos = droppable.offset();
    var newPos = {};

    if ( droppable.hasClass('dropzone') ) {
        newPos.top = dragPos.top - dropPos.top + 'px';
        newPos.left =  dragPos.left - dropPos.left + 'px';
    } else {
        newPos.top = ( dropPos.top - dragPos.top )* -1 + 'px';
        newPos.left = ( dropPos.left - dragPos.left )* -1  + 'px';
    }
    return newPos;
}

// Visually toggle the center divider in 42" open boxes
function toggleCenterDivider (e, ui, box) {
    var divide = box.siblings('svg')[0].querySelector('.divide');

    if (  divide === null ) return;
    else if ( ui.draggable.attr('data-furniture') === 'pullout-desk' &&
             !box.children('[data-furniture="pullout-desk"]').length ) {
        if ( e.type === 'dropover' ) {
            divide.classList.add('hide');
        } else {
            divide.classList.remove('hide');
        }
    }
}

// toggle dragggable snap options for standalone accessories
function toggleStandaloneSnap (e, ui) {
    var snapType = e.type === 'dropover' ? 'innerSnap' : 'outerSnap';
    var el = ui.draggable;
    var snap = el.draggable('option', snapType );
    var snapMode = el.draggable('option', snapType + 'Mode');
    var snapTolerance = el.draggable('option', snapType + 'Tolerance');

    // for updating the snapElements array
    el.draggable('option','snap', snap);
    el.draggable('option','snapTolerance', snapTolerance);
    el.draggable('option','snapMode', snapMode);
    // For reference: line 834 of node_modules/jquery-ui/draggable.js
    // starts at: $.ui.plugin.add("draggable", "snap", {
    var data = el.data("ui-draggable");
    var snapElements = $(data.options.snap).toArray();
    data.snapElements = mapPrune( snapElements, function(el) {
        var $t = $(el);
        var $o = $t.offset();
        if( el !== data.element[0]) {
            return {
                item: el,
                width: $t.outerWidth(),
                height: $t.outerHeight(),
                top: $o.top,
                left: $o.left
            };
        }
    });
}
// DRAG-AND-DROP FUNCTIONS
// create a dropzone (run when an item is placed in the dropzone)
function createDropbox (el) {
    var box = el.children().hasClass('dropbox') ? el.children('.dropbox') : el;
    var getDraggables = mapPrune( dragData, function (set) {
        if (  set.snapMode === 'inner' && box.is(set.snap) )
            return set.type;
    });

    box.droppable({
        accept: getDraggables.toString(),
        stack: '.dropped-item',
        greedy: true, // help stop dropzone from double cloning
        tolerance: 'pointer',
        activeClass: 'drop-active',
        over: function ( e, ui ) {
            if ( box.hasClass('dropbox') )
                toggleCenterDivider(e, ui, box);
            if ( ui.draggable.attr('data-acc') === 'standalone' )
                toggleStandaloneSnap(e, ui);
        },
        out: function ( e, ui ) {
            if ( box.hasClass('dropbox') )
                toggleCenterDivider(e, ui, box);
            if ( ui.draggable.attr('data-acc') === 'standalone' )
                toggleStandaloneSnap(e, ui);
        },
        drop: function ( e, ui ) {
            var dragItem = ui.helper;
            var dragOpts = ui.draggable.draggable('option');

            if ( dragItem.hasClass('panel-item') ) {
                dragItem = clone( dragItem, dragOpts);
            }
            //TODO: overhaul this
            if ( dragItem.children('.clickbox').length ) {
                console.log(ui.draggable.attr('data-name') + ' disabled');
                dragItem.draggable('disable');
            }

            dragItem
                .css(resetPosition( ui, this ))
                .appendTo(this);

            record.add(dragItem);
            console.log('data', record.onCanvas);
            rulers.set(record.onCanvas);
        }
    });
}

function createDropzone (querySelector) {
    return $(querySelector).droppable({
        accept: ':not([data-acc="inner"])',
        tolerance: 'fit',
        drop: function (e, ui ) {
            var dragItem = ui.helper;
            var dragOpts = ui.draggable.draggable('option');
            var isDropbox = ['box', 'hanging-file', 'pullout-desk'].some( function (type) {
                return dragItem.attr('data-furniture') === type;
            });
            // reveal / hide trashcan & rulers
            if ( !Object.keys(record.onCanvas).length ) {
                rulers.show();
                trash.show();
            }

            // clone it if its from the selection panel
            if ( dragItem.hasClass('panel-item') ) {
                dragItem = clone( dragItem, dragOpts );
                // set droppable settings for boxes to accept accessories
                if ( isDropbox ) createDropbox( dragItem );
            } // dropped-item is an accessory and was inside a box
            else if ( dragItem.parent().not(dropzone) )
            dragItem.css(resetPosition( ui, dropzone ));

            dragItem.appendTo(this);
            record.add(dragItem);
            rulers.set(record.onCanvas);
        }
    });
}
module.exports = createDropzone;
