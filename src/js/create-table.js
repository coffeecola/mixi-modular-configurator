module.exports = createTable;

// extract parts list from obj
function getArrOfParts (obj) {
    return Object.keys(obj).map( function (key) {
        var furniture = obj[key];
        var partsArr = [];

        partsArr.push(furniture.name);
        partsArr.push(furniture.color);
        if (furniture.inset) {
            partsArr.push( 'with ' + furniture.inset + ' inset');
        } else {
            partsArr.push('');
        }
        // Initial value before counting duplicates
        partsArr.push(1);
        return partsArr;
    });
}

function combineDuplicates (array) {
    return array.sort().filter( function (a, _,arr) {
        // combine matches
        var noMatch = true;
        arr.forEach( function (b) {
            // is not itself, and has same string value
            if (a !== b && a.join() === b.join()) {
                b[3]++; // add to 'Quantity' cell of this item
                noMatch = false;
            }
        });
        return noMatch;
    });
}

function createDomNode (tag, text) {
    tag = document.createElement(tag);
    text = document.createTextNode(text);
    tag.appendChild(text);
    return tag;
}

function createTable (obj, titles) {
    if (!Object.keys(obj).length) {
        return;
    } else {
        var table = document.createElement('table');
        var head = document.createElement('thead');
        var body = document.createElement('tbody');
        var partsList = getArrOfParts (obj);

        titles.forEach( function (title) {
            var th = createDomNode('th', title);
            head.appendChild(th);
        });

        // create table body
        partsList = combineDuplicates (partsList);
        partsList.forEach( function (row) {
            var tr = document.createElement('tr');
            row.forEach(function (cell) {
                var td = createDomNode('td', cell);
                tr.appendChild(td);
            });
            body.appendChild(tr);
        });
        table.appendChild(head);
        table.appendChild(body);
        return table;
    }
}
