var count = 0;

module.exports = {
    val: function () {
        return count;
    },
    add: function () {
        count++;
    },
    minus: function () {
        count--;
    }
};
