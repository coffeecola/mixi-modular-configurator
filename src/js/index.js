var $ = require('./j-weebly.js');
var instructions = require('./instructions.js');
var tabs = require('./tabs.js');
var makeDroppable = require('./drop.js');
var makeDraggable = require('./drag.js');
var record = require('./recorder.js');
var dropdowns = require('./dropdowns.js');
var createTable = require('./create-table.js');

// TODO: move all DOM nodes into index file?
// DOM nodes
// Initialize drag and drop
var $rulers = require('./rulers.js').dom;
var $dropzone = makeDroppable('.dropzone');
var $panelItems = makeDraggable('.panel-item');
var $canvas = $('.canvas');

// init()

// first user drag
$(document).on('dragstart', function () {
    var $instructions = $('.instructions');
    $instructions.addClass('hide');
    // no need to keep this event handler around after instructions are turned off
    $(this).off('dragstart');
});

$('.loading-screen').hide();
// TODO: potentially overhaul this code (disable/enable accessories inside boxes?)
$dropzone.on('mouseover', '.clickbox', function () {
    var dragItem = $(this).parent();
    if ( dragItem.data('uiDraggable') ) {
        dragItem
            .draggable('enable')
            .css('outline', '');
    }
});

$dropzone.on('mouseout', '.clickbox', function () {
    var dragItem = $(this).parent();
    var parentIsDropzone = dragItem.parent().hasClass('.dropzone');
    if ( dragItem.data('uiDraggable') && parentIsDropzone ) {
        dragItem
            .draggable('disable')
            .css('outline', '1px solid rgba(255,0,0,.3)');
    }
});

// ============================================================================
// TODO: capture print event (user hits CTRL/CMD + P)
$('#print').on('click', createPrintSheet );

function createPrintSheet () {
    /*
     * Weebly nests markup fairly deep, making it difficult to target the
     * display of print elements with @print-media CSS. Because of this, it
     * is easier to create a brand new div containing all of the elements that
     * will appear on the print-media and then remove it after the print job
     * is complete/cancelled.
     *
     * What we are doing here:
     * -----------------------
     * > Removing the Weebly stylesheet to stop collisions with print CSS
     * > Coping the masthead image
     * > Cloning the dropped furniture and rulers, correcting their coordinates
     * > Creating a table with all of the data from the furniture on canvas
     * > After the print job, restoring it to its previous state
     */

    // TODO: print when nothing on canvas?
    // TODO: scale if too large for canvas?
    // TODO: backgorund-image to foreground or pseudo-element?
    // TODO: ruler backgorund-images?

    var printSheet = document.createElement('div');
    var logo = $('.wsite-logo').find('img').clone();
    var $diagram = $('.canvas').clone(true);
    var weeblyCSS = $('link#wsite-base-style');
    var tableTitles = [ 'Piece', 'Color', 'Additional', 'Qty' ];
    var table = createTable(record.onCanvas, tableTitles);

    $diagram.find('.dropped-item')
        .each( function (_, el) {
            var furniture = $(el);
            var shift = {
                left: $rulers.find('.measure-X').position().left,
                top: $rulers.find('.measure-Y').position().top
            };
            var pos = {
            // using css method here since .position() requires the viewport
                left: parseFloat(furniture.css('left')),
                top: parseFloat(furniture.css('top'))
            };
            // ignore accessories that are inside boxes
            if ( furniture.parent().hasClass('dropzone') ) {
                furniture.css({
                    left: pos.left - shift.left,
                    top: pos.top - shift.top
                });
            }
        });
    // A lot of internal debate about whether to  use CSS !important instead
    $diagram.find('.measure-X').css('left', 0);
    $diagram.find('.measure-Y').css('top', 0);

    $diagram.find('.dropzone').css({
        width: $rulers.find('.measure-X').width() + 'px',
        height: $rulers.find('.measure-Y').height() + 'px'
    });
    // remove Weebly stylesheet
    weeblyCSS.remove();

    if ( table ) table.classList.add('parts-list');

    $(printSheet)
        .addClass('print')
        .append( logo, $diagram, table )
        .appendTo(document.body);

    window.print();
    $(document).on('mousemove', function () {
        // delete print items and unbind
        // once user returns from modal dialog
        var doc = $(this);
        $(printSheet).remove();
        doc.children('head').prepend(weeblyCSS);
        doc.off('mousemove');
    });
}

// $('.share').on('click', function () {
    // record in URI
    // create small URL
    // add to user's clipboard
// });

// $('.quote').on('click',function () {
    // show form
    // create table
    // add styles
    // add table to hidden input | replace if it has content
// });
