//TODO: refactor this module
var $ = require('./j-weebly.js');
var boundingArea = require('./bounding-area.js');

var $rulers = $('.measuring-tape');

function showRulers () {
   $rulers.removeClass('hide');
}

function hideRulers () {
    $rulers.addClass('hide');
}

function getBoundingArea (onCanvas) {
    // Collect all position objects of
    // dropped-items onCanvas into an array
    var result;
    var array = [];
    Object.keys(onCanvas).forEach(function dropGeometry (key) {
        var res = {};
        var obj = onCanvas[key];
        // Do not include accessories that are inside boxes
        if ( obj.parent === 'dropzone' ) {
            res.width  = obj.width;
            res.height = obj.height;
            res.left   = obj.left;
            res.top    = obj.top;
            array.push(res);
        }
    });
    result = array.length ? boundingArea(array) : {};
    return result;
}

// // creates span elements used as foot markers for the measuring tape
function spanFeet (tapeLength) {
    var span = '';
    var feet = ( tapeLength / 60 );

    // for each foot, create a span to be used as a marker
    for ( var i = 0; i <= feet; i++ ) {
        span += '<span class="foot-mark">' + i + '&#8202;' + '</span>';
    }
    return span;
}

function setRulers (onCanvas) {
    $rulers.children().each( function (i, ruler) {
        var markers;
        var cssValues = {};
        var area = getBoundingArea(onCanvas);
        $ruler = $(ruler);

        area.width = Math.round(area.width);
        area.height = Math.round(area.height);

        if ( $ruler.hasClass('measure-X') ) {
            cssValues.left = area.left + 'px';
            cssValues.width = area.width + 'px';
            markers = spanFeet(area.width);
        } else {
            cssValues.top = area.top + 'px';
            cssValues.height = area.height + 'px';
            markers = spanFeet(area.height);
        }
        $ruler.children('.foot-mark').remove();
        $ruler.css(cssValues).append(markers);
    });
}


module.exports = {
    dom: $rulers,
    show: showRulers,
    hide: hideRulers,
    set: function (onCanvas) {
        if ( Object.keys(onCanvas).length ) {
            setRulers(onCanvas);
        } else { hideRulers(); }
    }
};
