var $ = require('./j-weebly.js');
///////////////////////
// SELECTION PANEL TABS
///////////////////////
var tabs = $('.configurator-tabs a');
// INITIAL VALUES
var active = $(tabs[0]);
var content = $(active[0].hash);


function setActiveTab (newTab) {
    if ( active && content ) {
        active.removeClass('active');
        active.parent().removeClass('top-tab');
        content.hide();
    }
    active = $(newTab);
    content = $(newTab[0].hash);

    active.addClass('active');
    active.parent().addClass('top-tab');
    content.show();
}

// set initial tab
setActiveTab(active);
tabs.not(active).each(function () {
    $(this.hash).hide();
});

////////////////
// EVENT HANDLER
tabs.on('click', function(e){
    setActiveTab( $(this) );
    e.preventDefault();
});
