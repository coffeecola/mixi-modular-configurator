var fs = require('fs');
    file = process.argv[2];

if (!file) console.log('NO FILE SPECIFIED \n');

fs.readFile( file, 'utf8', function (err, data) {
  if (err) throw err;
  
  // place each symbol in array
  var arrOfSymbols = data.match(/<symbol(.|\n)+?<\/symbol>/g);

  arrOfSymbols.forEach( function (svgSymbol, i) {

    var svgID = (/id=?(?:\'|\")(.+?)(?:\'|\")/).exec(svgSymbol), // ID name becomes filename
        filename = svgID[1] + '.svg',
        contents = svgSymbol.replace(/symbol/g, 'svg');
    console.log(filename)
    // if ( i < 2) console.log(contents + '\n---\n');
  
    fs.writeFile( filename, contents, function (err) { 
      if (err) throw err;
      console.log(filename + 'written to disk');      
    });
  });
})